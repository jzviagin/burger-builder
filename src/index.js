import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import createSagaMiddleware from 'redux-saga'

import thunk from 'redux-thunk'

import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
//import counterReducer from './store/reducers/counter'
//import resultsReducer from './store/reducers/results'

import burgerBuilderReducer from './store/reducers/burgerBuilder'
import orderReducer from './store/reducers/order'
import authReducer from './store/reducers/auth'

import {Provider} from 'react-redux'



import {BrowserRouter} from 'react-router-dom'
import {watchAuth, watchBurgerBuilder, watchOrders} from './store/sagas'

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
    order:orderReducer,
    burgerBuilder:burgerBuilderReducer,
    auth: authReducer
})

const logger = store => {
    return (next) => {
        return action => {

            const result = next(action);
            return result;
        }
    }
}


const composeEnhancers = (process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null )|| compose;


const store = createStore(rootReducer , composeEnhancers(applyMiddleware(logger, thunk, sagaMiddleware)));

sagaMiddleware.run(watchAuth);
sagaMiddleware.run(watchOrders);
sagaMiddleware.run(watchBurgerBuilder);
const app = (
    <BrowserRouter>
        <App/>
    </BrowserRouter>
)


ReactDOM.render(<Provider store = {store}>{app}</Provider>, document.getElementById('root'));
//ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
