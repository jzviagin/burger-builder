import axios from 'axios'
import {put, delay, call} from 'redux-saga/effects'
import * as actions from '../actions';
export function*  logoutSaga(action){
    //yield localStorage.removeItem('token');
    //yield localStorage.removeItem('expDate');
    yield call([localStorage, "removeItem"], "token");
    yield call([localStorage, "removeItem"], "expDate");
    yield put(
        actions.authLogoutSucceed()
    )
}


export function*  checkAuthTimeoutSaga(action){
    yield delay((action.expireTime) * 1000);
    yield put(
        actions.authInitiateLogout()
    )
}


export function*  authUserSaga(action){
    //put(actions.authStart(action.username, action.password));
    let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyA9MZwf-qCktmIkrvnYrmVKglU2oQ2f0OQ';
    if (action.isLogin){
        url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA9MZwf-qCktmIkrvnYrmVKglU2oQ2f0OQ'
    }
    try{
        const res = yield axios.post(url, {

            email:	action.username,
            password:	action.password,
            returnSecureToken:	true

        });

        yield console.log(res)
        yield localStorage.setItem('token', res.data.idToken);
        let expDate = new Date(new Date().getTime() + (res.data.expiresIn * 1000));
        yield localStorage.setItem('expDate', expDate);
        yield put(actions.authSuccess(res.data.localId, res.data.idToken))
        yield put(actions.initiateAutoLogout(res.data.expiresIn));
        
    }catch(error){
        console.log(error)
        yield put(actions.authFail(error.response.data.error))
    }
   
       
    
}


export function* authCheckStateSaga(action){
    const token = yield localStorage.getItem('token');
    const expDate = yield new Date(localStorage.getItem('expDate'));
    if (!token){
        yield put(actions.authInitiateLogout());
    } else{
        if (new Date()> expDate){
            yield put(actions.authInitiateLogout());
        }else{
            try{
                const res= yield axios.post('https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyA9MZwf-qCktmIkrvnYrmVKglU2oQ2f0OQ', {

                idToken:	token
    
                })
                yield put(actions.authSuccess(res.data.users[0].localId, token))
                yield put(actions.initiateAutoLogout( (expDate.getTime() - new Date().getTime())/1000 ));

            }catch(error){
                yield put(actions.authFail(error.response.data.error))

            }
            
          
        }
    }
}