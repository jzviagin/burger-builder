import Axios from '../../axios-orders'
import {put, delay} from 'redux-saga/effects'
import * as actions from '../actions';


export function* initIngredientsSaga(){
    try{
    const res = yield Axios.get('/ingredients.json');
        yield put(actions.initIngredientsDone(res.data, false))
    }
    catch (error){
        yield put(actions.initIngredientsDone({}, true))

    }
}