import {logoutSaga, checkAuthTimeoutSaga, authUserSaga, authCheckStateSaga} from './auth'
import {initIngredientsSaga} from './burgerBuilder'
import {fetchOrdersStartSaga, purchaseBurgerSaga} from './orders'
import {takeEvery, all} from 'redux-saga/effects'
import * as actionTypes from '../actions/actionTypes';


export function* watchAuth(){
    yield all(
        [
             takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga),
             takeEvery(actionTypes.AUTH_INITIATE_AUTO_LOGOUT, checkAuthTimeoutSaga),
             takeEvery(actionTypes.AUTH_START, authUserSaga),
             takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga)
        ]);
  /*  yield takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga);
    yield takeEvery(actionTypes.AUTH_INITIATE_AUTO_LOGOUT, checkAuthTimeoutSaga);
    yield takeEvery(actionTypes.AUTH_START, authUserSaga);
    yield takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga);

    yield takeEvery(actionTypes.INIT_INGREDIENTS_START, initIngredientsSaga);
    yield takeEvery(actionTypes.FETCH_ORDERS_START, fetchOrdersStartSaga);
    yield takeEvery(actionTypes.PURCHASE_BURGER_START, purchaseBurgerSaga);*/
    

}

export function* watchOrders(){
    yield all(
        [
        
             takeEvery(actionTypes.FETCH_ORDERS_START, fetchOrdersStartSaga),
             takeEvery(actionTypes.PURCHASE_BURGER_START, purchaseBurgerSaga)
        ]);
  /*  yield takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga);
    yield takeEvery(actionTypes.AUTH_INITIATE_AUTO_LOGOUT, checkAuthTimeoutSaga);
    yield takeEvery(actionTypes.AUTH_START, authUserSaga);
    yield takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga);

    yield takeEvery(actionTypes.INIT_INGREDIENTS_START, initIngredientsSaga);
    yield takeEvery(actionTypes.FETCH_ORDERS_START, fetchOrdersStartSaga);
    yield takeEvery(actionTypes.PURCHASE_BURGER_START, purchaseBurgerSaga);*/
    

}


export function* watchBurgerBuilder(){
    yield all(
        [      
             takeEvery(actionTypes.INIT_INGREDIENTS_START, initIngredientsSaga)
        ]);
  /*  yield takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga);
    yield takeEvery(actionTypes.AUTH_INITIATE_AUTO_LOGOUT, checkAuthTimeoutSaga);
    yield takeEvery(actionTypes.AUTH_START, authUserSaga);
    yield takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga);

    yield takeEvery(actionTypes.INIT_INGREDIENTS_START, initIngredientsSaga);
    yield takeEvery(actionTypes.FETCH_ORDERS_START, fetchOrdersStartSaga);
    yield takeEvery(actionTypes.PURCHASE_BURGER_START, purchaseBurgerSaga);*/
    

}