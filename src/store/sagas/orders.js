import Axios from '../../axios-orders'
import {put, delay} from 'redux-saga/effects'
import * as actions from '../actions';


export function* purchaseBurgerSaga(action){
    try{
        const response = yield Axios.post('/orders.json?auth=' + action.token, action.orderData);
        //this.props.history.push('/' )
        yield put(actions.purchaseBurgerSuccess(response.data.name, action.orderData))
    }catch(error){

        yield put(actions.purchaseBurgerFail(error));

    }
}


export function* fetchOrdersStartSaga(action){
    //dispatch(fetchOrdersStart());
    try{
        const res = yield Axios.get('/orders.json?auth=' + action.token + '&orderBy="userId"&equalTo="'+ action.userId+ '"')
        const fetchedOrders = [];
        for (let key in res.data){
            yield fetchedOrders.push({
                ...res.data[key],
                id:key
            }
            
                );
        }
        yield put(actions.fetchOrdersSuccess(fetchedOrders))

    }catch (error){
        yield console.log(error)
        yield put(actions.fetchOrdersFail(error))

    }
    
}