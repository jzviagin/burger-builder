import * as actionTypes from '../actions/actionTypes';
import axios from 'axios'


export const authStart = (username, password) => {
    return {
        type: actionTypes.AUTH_START,
        username: username,
        password: password
        
    }
}


export const authSuccess = ( userId, token) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        userId: userId,
        token:token
    }
}

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const authInitiateLogout = ()=>{
    //localStorage.removeItem('token');
    //localStorage.removeItem('expDate');
    return {
        type: actionTypes.AUTH_INITIATE_LOGOUT,
    }
}


export const authLogoutSucceed = ()=>{
    return {
        type: actionTypes.AUTH_LOGOUT_SUCCEED,
    }
}

export const authSetRedirectPath = (path)=>{
    return {
        type: actionTypes.AUTH_SET_REDIRECT_PATH,
        path: path
    }
}

export const initiateAutoLogout = (expireTime) =>{
    return {
        type: actionTypes.AUTH_INITIATE_AUTO_LOGOUT,
        expireTime: expireTime
    }

}


export const auth = (username, password, isLogin) => {
    return {
        type:actionTypes.AUTH_START,
        username: username,
        password:password,
        isLogin:isLogin
    }
   /* return (dispatch, getState)=>{
        dispatch(authStart(username, password));
        let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyA9MZwf-qCktmIkrvnYrmVKglU2oQ2f0OQ';
        if (isLogin){
            url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA9MZwf-qCktmIkrvnYrmVKglU2oQ2f0OQ'
        }
        axios.post(url, {

            email:	username,
            password:	password,
            returnSecureToken:	true

        }).then(res => {
            localStorage.setItem('token', res.data.idToken);
            let expDate = new Date(new Date().getTime() + (res.data.expiresIn * 1000));
            localStorage.setItem('expDate', expDate);
            dispatch(authSuccess(res.data.localId, res.data.idToken))
            dispatch(initiateAutoLogout(res.data.expiresIn));
        }).catch(error =>{
            dispatch(authFail(error.response.data.error))
        });
    }*/
}


export const authCheckState = () =>{

    return {
        type:actionTypes.AUTH_CHECK_STATE

    }
   /* return dispatch =>{
        const token = localStorage.getItem('token');
        const expDate = new Date(localStorage.getItem('expDate'));
        if (!token){
            dispatch(authInitiateLogout());
        } else{
            if (new Date()> expDate){
                dispatch(authInitiateLogout());
            }else{
            
                axios.post('https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyA9MZwf-qCktmIkrvnYrmVKglU2oQ2f0OQ', {

                    idToken:	token
        
                }).then(res => {
                    dispatch(authSuccess(res.data.users[0].localId, token))
                    dispatch(initiateAutoLogout( (expDate.getTime() - new Date().getTime())/1000 ));
                }).catch(error =>{
                    dispatch(authFail(error.response.data.error))
                });
            }
        }
    }*/
}