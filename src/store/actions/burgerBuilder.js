import * as actionTypes from '../actions/actionTypes'
import Axios from '../../axios-orders'

export const addIngredient = (name) => {
    return{
        type: actionTypes.ADD_INGREDIENT,
        ingredient: name
    }
}


export const removeIngredient = (name) => {
    return{
        type: actionTypes.REMOVE_INGREDIENT,
        ingredient: name
    }
}

export const initIngredientsStart = ()=>{
    return{
        type: actionTypes.INIT_INGREDIENTS_START
    }/*
    return (dispatch, getState)=>{

        Axios.get('/ingredients.json').then(res => {
            dispatch({
                type: actionTypes.INIT_INGREDIENTS,
                ingredients: res.data,
                error:false
            })
        }).catch(error =>{
            dispatch({
                type: actionTypes.INIT_INGREDIENTS,
                ingredients: {},
                error:true
            })
        });
    }*/
}


export const initIngredientsDone = (ingredients, error)=>{
    return{
        type: actionTypes.INIT_INGREDIENTS_DONE,
        ingredients: ingredients,
        error: error

    }
}

