import * as actionTypes from '../actions/actionTypes';




export const purchaseBurgerSuccess = (id, orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_SUCCESS,
        orderId: id,
        orderData:  orderData
    }
}

export const purchaseBurgerFail = (error) => {
    return {
        type: actionTypes.PURCHASE_BURGER_FAIL,
        error: error
    }
}


export const purchaseBurgerStart = (orderData, token) => {
    return {
        type: actionTypes.PURCHASE_BURGER_START,
        orderData: orderData,
        token: token

    }
}


export const purchaseInit = () => {
    return {
        type: actionTypes.PURCHASE_INIT
    }
}

/*
export const purchaseBurger = (orderData, token) => {

    return {
        type: actionTypes.PURCHASE_INIT
    }


    return (dispatch, getState)=>{


      dispatch(purchaseBurgerStart());
        Axios.post('/orders.json?auth=' + token, orderData).then(response=>{
            //this.props.history.push('/' )
            dispatch(purchaseBurgerSuccess(response.data.name, orderData))
        }).catch(error => {

           dispatch(purchaseBurgerFail(error))
    
        });
       
    }
}*/


/*export const fetchOrdersStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_START
        
    }
}*/

export const fetchOrdersSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        orders:orders
    }
}

export const fetchOrdersFail = (error) => {
    return {
        type: actionTypes.FETCH_ORDERS_FAIL,
        error: error
    }
}




export const fetchOrdersStart = (token, userId) => {
    return {
        type: actionTypes.FETCH_ORDERS_START,
        token: token,
        userId: userId

    }
}

/*export const fetchOrders = (token, userId) => {
    return (dispatch, getState)=>{
        dispatch(fetchOrdersStart());

        Axios.get('/orders.json?auth=' + token + '&orderBy="userId"&equalTo="'+ userId+ '"').then(res => {
            const fetchedOrders = [];
            for (let key in res.data){
                fetchedOrders.push({
                    ...res.data[key],
                    id:key
                }
                
                    );
            }
            dispatch(fetchOrdersSuccess(fetchedOrders))
        }).catch(error =>{
            dispatch(fetchOrdersFail(error))
        });
    }
}*/
