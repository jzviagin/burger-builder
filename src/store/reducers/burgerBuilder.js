import * as actionTypes from '../actions/actionTypes'

import {updateObject} from '../../shared/utility'


const initialState = {

    totalPrice:4,
    purchasable: false
}


const INGREDIENT_PRICES = {
    salad: 0.5,
    bacon: 0.7,
    cheese: 0.4,
    meat: 1.3
}


const addIngredient = (state, action) =>{
    let newIngredients = {...state.ingredients};
    newIngredients[action.ingredient] = newIngredients[action.ingredient] + 1;
    const totalIngredients = Object.keys(newIngredients).map(key => newIngredients[key]).reduce( (sum, element) =>{
        return sum + element;
    }, 0);

    return updateObject(state, {
        ingredients: newIngredients,
        totalPrice: Number.parseFloat((state.totalPrice + INGREDIENT_PRICES[action.ingredient]).toFixed(2)),
        purchasable: totalIngredients > 0
    })
}

const initIngredients = (state, action) =>{
    if (action.error){
        return updateObject(state, {
            error: true
        })
    }

    return updateObject(state, {
        ingredients: action.ingredients,
        totalPrice: initialState.totalPrice,
        error: false
    })
}

const removeIngredient = (state, action) =>{
    let newIngredients2 = {...state.ingredients};
    newIngredients2[action.ingredient] = newIngredients2[action.ingredient] - 1;
    const totalIngredients2 = Object.keys(newIngredients2).map(key => newIngredients2[key]).reduce( (sum, element) =>{
        return sum + element;
    }, 0);

    return updateObject(state, {
        ingredients: newIngredients2,
        totalPrice: Number.parseFloat((state.totalPrice - INGREDIENT_PRICES[action.ingredient]).toFixed(2)),
        purchasable: totalIngredients2 > 0
    })
}

const reducer = (state = initialState,action) => {





    switch (action.type){
        case actionTypes.ADD_INGREDIENT:
           return addIngredient(state, action);
        
        
        case actionTypes.REMOVE_INGREDIENT:
           return removeIngredient(state, action);

        case actionTypes.INIT_INGREDIENTS_DONE:
           return initIngredients(state, action);
            
    

        default:
            return state;
    }
    
    
};

export default reducer;