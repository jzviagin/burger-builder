import reducer from './auth'
import * as actionTypes from '../actions/actionTypes';

describe('auth reducer', ()=>{
    it('should return the initial state', ()=>{
        expect(reducer(undefined, {})).toEqual({
            

                loading: false,
                token: null,
                userId: null,
                error: null,
                redirectPath: "/"
            
        });
    })

    it('should store token upon login', ()=>{
        expect(reducer({
            

            loading: false,
            token: null,
            userId: null,
            error: null,
            redirectPath: "/"
        
        }, {
            type: actionTypes.AUTH_SUCCESS,
            userId: "aaa",
            token:"bbb"
        })).toEqual({
            

                loading: false,
                token: "bbb",
                userId: "aaa",
                error: null,
                redirectPath: "/"
            
        });
    })
})