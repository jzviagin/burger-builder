import * as actionTypes from '../actions/actionTypes'
import {updateObject} from '../../shared/utility'




const initialState = {

    loading: false,
    token: null,
    userId: null,
    error: null,
    redirectPath: "/"
}


const authSuccess = (state, action)=>{
    return updateObject(state, {
        loading: false,
        error:null,
        userId: action.userId,
        token:action.token
    });
}

const authFail = (state, action)=>{
    return updateObject(state, {
        loading: false,
        error: action.error
    });
}

const authStart = (state, action)=>{
    return updateObject(state, {
        loading: true
    });
}

const authLogoutSucceed = (state, action)=>{
    return updateObject(state, {
        userId: null,
        token: null
    });
}



const authSetRedirectPath = (state, action)=>{
    return updateObject(state, {
        redirectPath: action.path
    });
}

const reducer = (state = initialState,action) => {

   switch (action.type){

        case actionTypes.AUTH_SUCCESS:
            return authSuccess(state, action)
        case actionTypes.AUTH_FAIL:
            return authFail(state, action);
        case actionTypes.AUTH_START:
           return authStart(state, action);
        case actionTypes.AUTH_LOGOUT_SUCCEED:
            return authLogoutSucceed(state, action);
        case actionTypes.AUTH_SET_REDIRECT_PATH:
            return authSetRedirectPath(state, action);
        default:
            return state;
    }
    
    
}

export default reducer;