
import React, { useState } from 'react';

import classes from './Order.css'




const order = (props) =>{

    const ingredients = [];
    for (let ingredientName in props.ingredients){
        ingredients.push({
            name: ingredientName,
            amount: props.ingredients[ingredientName]
        })
    }
    const ingredientsOutput = ingredients.map(ingredient => {
        return (
            <span
                style={{textTransform: 'capitalize', 
                display: 'inline-block',
                margin: '0 8px',
                border: '1px solid #ccc',
                padding: '5px'}}
            >{ingredient.name}({ingredient.amount})</span>
            )
        })
    return (


            <div className = {classes.Order}>
                <div style ={{
                    display:'flex',
                    flexDirection: 'row'
                }}>
                    <span>Ingredients: {ingredientsOutput}</span>
                    <div style={{
                        display:'flex',
                        flex:1,
                        justifyContent:'flex-end'
                    }}><button onClick = {props.showBurgerClicked} className={classes.ShowBurgerButton}>
                        Show Burger
                    </button>
                    </div>
                </div>

                
        <p>price: <strong>${props.price}</strong></p>
            </div>
        )
}




export default order;