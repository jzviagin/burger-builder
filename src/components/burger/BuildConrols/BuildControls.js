import React from 'react';
import classes from './BuildControls.css'
import BuildControl from './BuildControl/BuildControl'

const controls = [
    {
        label: 'Salad',
        type:'salad'
    },
    {
        label: 'Meat',
        type:'meat'
    },
    {
        label: 'Cheese',
        type:'cheese'
    },
    {
        label: 'Bacon',
        type:'bacon'
    }
]

const buildControls = (props)=>{
    return (
        <div className = {classes.BuildControls}>
            <p>Current Price: <strong>${props.price}</strong></p>
            {
                controls.map( (elem) => <BuildControl key = {elem.label} removeDisabled = {props.disabledInfo[elem.type] === true} ingredientAdded = {props.ingredientAdded} 
                ingredientRemoved = {props.ingredientRemoved} label = {elem.label} type = {elem.type}></BuildControl>)
            }
            <button className = {classes.OrderButton} disabled = {props.purchasable === false}
            onClick = {props.purchaseHandler}> 
                {props.isAuth? "ORDER NOW":"Click to authenticate"}
            </button>
        </div>
    );
}

export default buildControls;
