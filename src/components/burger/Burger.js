import React from 'react';
import BurgerIngredient from './BurgerIngredients/BurgerIngredient'
import {withRouter} from 'react-router-dom';

import classes from './Burger.css'

/*const repeat = (value, count) =>{
    let i;
    let ret = [];
    for ( i = 0 ; i < count ; i++){
        ret.push(value)
    }
    return ret;
}*/

const burger = (props)=>{
    let transformedIngredients = Object.keys(props.ingredients).map(key => {
        //return repeat( <BurgerIngredient type = {key} />, props.ingredients[key])
        return [...Array(props.ingredients[key])].map((_, index) => <BurgerIngredient key = {key + index} type = {key} />)
       
    }).reduce((arr, el) => {
        return arr.concat(el)
    }, [])
    if (transformedIngredients.length ===0){
        transformedIngredients = <p> please add ingredients 1st</p>;
    }
    return  (
        <div className = {classes.Burger}>
            <BurgerIngredient type = 'bread-top' />
            {transformedIngredients}
            <BurgerIngredient type = 'bread-bottom' />
        </div>
    )
}

export default withRouter(burger);