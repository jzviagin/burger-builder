import React from 'react';
import Aux from '../../../hoc/AuxComponent'

import Button from '../../UI/Button/Button'

const orderSummary = (props)=>{





    const ingredientSummary = Object.keys(props.ingredients).map(elem => (
        <li key= {elem}><span style = {{textTransform: 'capitalize'}}>{elem}</span>: {props.ingredients[elem]}</li>
    ))


    return (
        
        <Aux>
            <h3>Your Order</h3>
            <p>A delicious burger with the following ingredients:</p>
            <ul>
                {ingredientSummary}
            </ul>
            <p><strong>Total Price: ${props.totalPrice}</strong></p>
            <p>Continue to Checkout?</p>
            <Button btnType = 'Danger' clicked = {props.cancelPurchaseHandler}>CANCEL</Button>
            <Button btnType = 'Success' clicked = {props.continuePurchaseHandler}>CONTINUE</Button>
        </Aux>
    );
    

}



export default orderSummary;