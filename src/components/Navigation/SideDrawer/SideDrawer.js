import classes from './SideDrawer.css'

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Backdrop from '../../UI/Backdrop/Backdrop'
import Aux from '../../../hoc/AuxComponent'


import React from 'react';



const sideDrawer = (props) => {

   

    return (
        <Aux>
            <Backdrop show = {props.isOpen} clicked = {props.backDropClicked}/>
            <div className = {[classes.SideDrawer, props.isOpen ? classes.Open: classes.Closed].join(' ') } onClick = {props.backDropClicked} > 
                    <div className={classes.Logo}>
                    <Logo />
                </div>
                <nav>
                    <NavigationItems isAuth = {props.isAuth}/>
                </nav>
        
            </div>
        </Aux>
     

    );
}


export default sideDrawer;