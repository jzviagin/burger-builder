
import React, {  useState } from 'react';

import Aux from '../AuxComponent'

import classes from './Layout.css'

import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer'

import {connect} from 'react-redux'




const layout = (props)=>{



    const [isOpen, setIsOpen] = useState(false)

 

   

    const menuClickedHandler = () => {
        if (isOpen === false){
            setIsOpen(true) ;
        }else{
            setIsOpen(false) ;
        }
    }

    const closeSideDrawerHandler = () => {
        setIsOpen(false);
    }



   



    return(
        <Aux>
        <div> Burger</div>
        <Toolbar isAuth = {props.token != null} menuClicked = {menuClickedHandler}></Toolbar>
        <SideDrawer isAuth = {props.token != null} isOpen={isOpen} backDropClicked = {closeSideDrawerHandler}/>
        <main className = {classes.Content}>
            {props.children}
        </main>
    </Aux>
    )
    
}



const mapStateToProps = (state) =>{
    return {
        token: state.auth.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(layout);
