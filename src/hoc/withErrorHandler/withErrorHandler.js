import React from 'react';
import Modal from '../../components/UI/Modal/Modal'
import Aux from '../AuxComponent'
import useHttpErrorHandler from '../../hooks/http-error-handler'

const withErrorHandler = (WrappedComponent, axios) => {
    return (props)=>{
        console.log('withErrorHandler render started');
        
        const [error, clearError] = useHttpErrorHandler(axios);

        const backdropClickedHandler = ()=>{
            clearError();
        }

       

            
        console.log('withErrorHandler render done');
      
        return(                
            <Aux>
                <Modal show = {error} backdropClickedHandler = {backdropClickedHandler}> {error ? error.message: ''} </Modal>
                <WrappedComponent {...props}/>
            </Aux>)

        

        

    }
    
}

export default withErrorHandler;