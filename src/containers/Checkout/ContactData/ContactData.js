
import React, { useState } from 'react';

import Button from '../../../components/UI/Button/Button'


import Spinner from '../../../components/UI/Spinner/Spinner'
import Input from '../../../components/UI/Input/Input'
import * as actionCreators from '../../../store/actions/'
import {connect} from 'react-redux'
import classes from './ContactData.css'


import {updateObject, checkValidity} from '../../../shared/utility'

const contactData = (props)=>{


    const [orderForm, setOrderForm] = useState({
        name:{
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your Name'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        street: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Street'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        zipCode:  {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Zip Code'
            },
            value: '',
            validation: {
                required: true,
                minLength: 5,
                maxLength: 5
            },
            valid: false,
            touched: false
        },
        country: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Country'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        email: {
            elementType: 'input',
            elementConfig: {
                type: 'email',
                placeholder: 'Email'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        deliveryMethod: {
            elementType: 'select',
            elementConfig: {
                options: [
                    {value: 'fastest', displayValue: 'Fastest'},
                    {value: 'cheapest', displayValue: 'Cheapest'}
                ]
            },
            value: 'fastest'
        }
    });

    const [formIsValid, setFormIsValid] = useState(false);

    const inputChangedHandler = (event, inputIdentifier)=>{
        const updatedFormElement = updateObject(orderForm[inputIdentifier],  {
            value: event.target.value,
            valid: orderForm[inputIdentifier].validation?  checkValidity(event.target.value, orderForm[inputIdentifier].validation): orderForm[inputIdentifier].valid,
            touched: true
        })

        const newOrderForm = updateObject(orderForm, {
            [inputIdentifier]: updatedFormElement
        })
        newOrderForm[inputIdentifier] = updatedFormElement;
        let isValid = true;
        for (let index in newOrderForm){
            if (newOrderForm[index].validation){
                if (!newOrderForm[index].valid) {
                    isValid = false;
                }
            }
        }

        setOrderForm(newOrderForm);
        setFormIsValid(isValid);
    }

    const orderClickedHandler = (event)=>{
        event.preventDefault();




        const formData = {};

        for ( let orderElement in orderForm){
            formData[orderElement] = orderForm[orderElement].value;
        }


        const order = {
            ingredients: props.ingredients,
            price: props.totalPrice,
            orderData: formData,
            userId: props.userId
        }


     
        props.onPurchaseBurgerStart(order, props.token);
      

    }




    const inputs = [];
    for (let inputName in orderForm){
        inputs.push(<Input key={inputName} elementType = {orderForm[inputName].elementType} elementConfig = {orderForm[inputName].elementConfig}
            value = {orderForm[inputName].value}
            invalid = {!orderForm[inputName].valid}
            shouldValidate = {orderForm[inputName].validation}
            touched = {orderForm[inputName].touched}
            changed= {(event) => {
                return inputChangedHandler(event, inputName)}}/>)
    }
    return (
        props.loading?<Spinner/>:
        
        <div className={classes.ContactData}>
            <h4>Enter your contact data</h4>
            <form>
                {inputs}
                <Button btnType = "Success" disabled= {!formIsValid} clicked={orderClickedHandler}>ORDER</Button>
            </form>
        </div>
    )
    
}

//export default ContactData


const mapStateToProps = (state) =>{
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onPurchaseBurgerStart: (orderData, token) => dispatch(actionCreators.purchaseBurgerStart (orderData, token))
        //onAddIngredient: (ingredient)=> dispatch({type:actionTypes.ADD_INGREDIENT, ingredient: ingredient}),
       // onRemoveIngredient: (ingredient)=> dispatch({type:actionTypes.REMOVE_INGREDIENT, ingredient: ingredient})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(contactData);