
import React from 'react';




import {Route, Redirect} from 'react-router-dom'

import ContactData from './ContactData/ContactData'

import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary'



import {connect} from 'react-redux'


const checkout = (props)=>{







    const checkoutCanceledHandler = ()=>{
        props.history.goBack();
    }

    const checkoutContinuedHandler = ()=>{
        props.history.replace('/checkout/contact-data')
    }



    let summary = <Redirect to="/"/>
    if (!props.purchased && props.ingredients){
        summary = <CheckoutSummary 
        ingredients={props.ingredients} 
        checkoutCanceled = {checkoutCanceledHandler}
        checkoutContinued = {checkoutContinuedHandler}
    />
    }
    return (
        <div>
            {summary}

        

            <Route 
                path={props.match.path + '/contact-data' } 
                //render={(props) => <ContactData ingredients = {this.props.ingredients} totalPrice = {this.props.totalPrice} {...props}/>} 
                component={ContactData}
            />
        
            
        </div>
    )
    
}


const mapStateToProps = (state) =>{
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        purchasable: state.burgerBuilder.purchasable,
        purchased: state.order.purchased
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //onPurchaseInit: ()=>dispatch(actionTypes.purchaseInit())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(checkout);