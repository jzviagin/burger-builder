import React, { useState} from 'react';

import Button from '../../components/UI/Button/Button'


import * as actionCreators from '../../store/actions/'

import Input from '../../components/UI/Input/Input'


import {connect} from 'react-redux'


import classes from './Auth.css'

import {Redirect} from  'react-router-dom'

import {updateObject, checkValidity} from '../../shared/utility'

import Spinner from '../../components/UI/Spinner/Spinner'


const auth = (props)=>{


    const [controls, setControls] = useState({
        email:{
            elementType: 'input',
            elementConfig: {
                type: 'email',
                placeholder: 'Mail Address'
            },
            value: '',
            validation: {
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        },
        password:{
            elementType: 'input',
            elementConfig: {
                type: 'password',
                placeholder: 'Password'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        }
    });
    const [formIsValid, setFormIsValid] = useState(false);
    const [isLogin, setIsLogin] = useState(false);



    const inputChangedHandler = (event, inputIdentifier)=>{


        const newControl = updateObject(controls[inputIdentifier], {
            touched: true,
            value: event.target.value,
            valid: controls[inputIdentifier].validation? checkValidity(event.target.value, controls[inputIdentifier].validation): controls[inputIdentifier].valid
        })

       const newControls = updateObject(controls, {
           [inputIdentifier]: newControl
       })

        let isValid = true;
        for (let index in newControls){
            if (newControls[index].validation){
                if (!newControls[index].valid) {
                    isValid = false;
                }
            }
        }
        setControls(newControls);
        setFormIsValid(isValid)
    
    }


    



    if(props.token != null){
        return <Redirect  to={props.redirectPath}/>
    }

    if (props.loading === true) {
        return <Spinner/>
    }

    let errorMessage = null;
    if (props.error){
    errorMessage = (<p>{props.error.message}</p>)
    }
    const inputs = [];
    for (let inputName in controls){
        inputs.push(<Input key={inputName} elementType = {controls[inputName].elementType} elementConfig = {controls[inputName].elementConfig}
            value = {controls[inputName].value}
            invalid = {!controls[inputName].valid}
            shouldValidate = {controls[inputName].validation}
            touched = {controls[inputName].touched}
            changed= {(event) => {
                return inputChangedHandler(event, inputName)}}/>)
    }
    return (
        
        <div className={classes.Auth}>
            {errorMessage}
            <form>
                {inputs}
                <Button btnType = "Success" disabled= {!formIsValid}  clicked= {
                    ()=>{
                        props.onAuth(controls.email.value, controls.password.value, isLogin);
                    }
                }>Submit</Button>
            </form>
            <Button btnType = "Danger" disabled= {false}  clicked= {
                    ()=>{
                        setIsLogin(!isLogin)
                    
                    }
                }>{isLogin? 'Switch to sign up':'Switch to log in'}</Button>
        </div>
    )
    
}



const mapStateToProps = (state) =>{
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        token: state.auth.token,
        redirectPath: state.auth.redirectPath
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onAuth: (username, password, isLogin) => dispatch(actionCreators.auth (username, password, isLogin))

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(auth);

