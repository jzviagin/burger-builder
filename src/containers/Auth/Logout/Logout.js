
import React, { useEffect, useState } from 'react';

import {Redirect} from 'react-router-dom';







import * as actionCreators from '../../../store/actions/'


import {connect} from 'react-redux'









const logout = (props)=>{


    const {onLogout} = props;

    useEffect(()=>{
        onLogout();
    },[onLogout])
  
    

    return (<Redirect to='/'/>);
    

}



const mapStateToProps = (state) =>{

    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogout: () => {
            dispatch(actionCreators.authInitiateLogout ())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(logout);

