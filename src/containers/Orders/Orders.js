
import React, { useEffect, useState } from 'react';

import Order from '../../components/Order/Order'

import Axios from '../../axios-orders'

import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'

import Spinner from '../../components/UI/Spinner/Spinner'

import * as actionCreators from '../../store/actions/'

import {connect} from 'react-redux'

import {Redirect} from 'react-router-dom'

import EmptyOrders from './EmptyOrders'

import Modal from '../../components/UI/Modal/Modal'

import Aux from '../../hoc/AuxComponent'

import Burger from '../../components/burger/Burger'







const orders = (props)=>{

    const [ingredients, setIngredients] = useState(null)

    const {onFetchOrders} = props

    useEffect(()=>{
        onFetchOrders(props.token, props.userId);
    },[onFetchOrders])
  
    

    if(props.token == null){
        return <Redirect to = "/"/>
    }

    if (props.loading === true) {
        return <Spinner/>
    }



    if (props.orders.length == 0) {
        return (
            <EmptyOrders/>
        )
    }



    return (
        <Aux>
            <Modal show = {ingredients != null}
                        backdropClickedHandler={()=>{
                            setIngredients(null);
                        }} >
                            {ingredients? <Burger ingredients = {ingredients}/> :null }
                        </Modal>
            <div>
                {
                    props.orders.map(order => {
                        return <Order key= {order.id}
                        ingredients={order.ingredients}
                        price = {order.price}
                        showBurgerClicked = {()=>{
                            setIngredients(order.ingredients)
                        }}
                    />
                    })
                }
            
            </div>
        </Aux>
        
    )
    

}



const mapStateToProps = (state) =>{
    return {
        orders: state.order.orders,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchOrders: (token, userId) => dispatch(actionCreators.fetchOrdersStart (token, userId))
        //onAddIngredient: (ingredient)=> dispatch({type:actionTypes.ADD_INGREDIENT, ingredient: ingredient}),
       // onRemoveIngredient: (ingredient)=> dispatch({type:actionTypes.REMOVE_INGREDIENT, ingredient: ingredient})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(orders, Axios));

