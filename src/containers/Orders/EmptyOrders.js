
import React from 'react';

import classes from './EmptyOrders.css'
import Button from '../../components/UI/Button/Button'
//import { useNavigate } from "react-router-dom";
import { useHistory } from "react-router-dom";


const emptyOrders = (props)=>{

    const history = useHistory();
    return (
        <div className = {classes.Container}>
            <span>No orders yet</span>
            <button className={classes.CreateBurgerButton} disabled= {false}  onClick= {
                    ()=>{
                        history.push("/");
                    }
                }>Crete a Burger</button>
        </div>
    )
}




export default emptyOrders;

