import React, { useState, useEffect, useCallback } from 'react';
import Aux from '../../hoc/AuxComponent'
import Burger from '../../components/burger/Burger'
import BuildControls from '../../components/burger/BuildConrols/BuildControls'

import Modal from '../../components/UI/Modal/Modal'

import Axios from '../../axios-orders'

import Spinner from '../../components/UI/Spinner/Spinner'

import OrderSummary from '../../components/burger/OrderSummary/OrderSummary'

import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'

import * as actionCreators from '../../store/actions/'

import {connect, useDispatch, useSelector} from 'react-redux'



/*const INGREDIENT_PRICES = {
    salad: 0.5,
    bacon: 0.7,
    cheese: 0.4,
    meat: 1.3
}*/

const burgerBuilder = (props) =>{



    const [purchasing, setPurchasing] = useState(false);
    //const [requestSent, setRequestSent] = useState(false);
    const [openSideDrawer, setOpenSideDrawer] = useState(false);

    const dispatch = useDispatch();
    const onAddIngredient =  (ingredient)=> dispatch(actionCreators.addIngredient(ingredient))
    const onRemoveIngredient =  (ingredient)=> dispatch(actionCreators.removeIngredient(ingredient))
    const onInitIngredients = useCallback(()=> dispatch(actionCreators.initIngredientsStart()), [])
    const onPurchaseInit = ()=>dispatch(actionCreators.purchaseInit())
    const onAuthSetRedirectPath = useCallback((path)=> dispatch(actionCreators.authSetRedirectPath(path)), [])

    const ingredients = useSelector(state => state.burgerBuilder.ingredients);
    const totalPrice = useSelector(state => state.burgerBuilder.totalPrice);
    const purchasable = useSelector(state => state.burgerBuilder.purchasable);
    const loadError = useSelector(state => state.burgerBuilder.error);
    const token = useSelector(state => state.auth.token);

    //const {onInitIngredients,
   //         onAuthSetRedirectPath} =props

    useEffect(()=>{
        onInitIngredients()
        onAuthSetRedirectPath('/');
    },[onInitIngredients, onAuthSetRedirectPath])

    




    //initialState = this.state;

  

 

    const menuClickedHandler=() =>{
        if (openSideDrawer === false){
            setOpenSideDrawer(true);
        }else{
            setOpenSideDrawer(false);
        }
     
    }

    const closeSideDrawerHandler = () => {
        setOpenSideDrawer(false);
    }



    const purchaseHandler = ()=> {
        if (token == null){
            onAuthSetRedirectPath('/checkout');
            props.history.push('/login');
            return;
        }
        setPurchasing(true);
        
    }

    const cancelPurchaseHandler=()=>{
        setPurchasing(false);
    }

   /* const continuePurchaseHandlerrrr(){
        const order = {
            ingredients: this.state.ingredients,
            price: this.state.price,
            customer:{
                name: 'Max',
                address: {
                    street: 'test street',
                    zipCode: '12345',
                    country: 'Germany'
                },
                email: 'test@test.com'
            },
            deliveryMethod: 'fastest'
        }
        Axios.post('/orders.json', order).then(response=>{
            this.setState({
                ...this.initialState
            })
        }).catch(error => {
        });
        this.setState({
            requestSent: true
        })
    }*/




    if(loadError){
        return <p>ingredients can't be loaded</p>
    }
    if (!ingredients){
        return <Spinner/>
    }




    let disabledInfo = {};
    let orderSummary =  <OrderSummary ingredients = {ingredients} cancelPurchaseHandler = {cancelPurchaseHandler}
    continuePurchaseHandler = {()=>{
        onPurchaseInit();
        props.history.push('/checkout');
    }}
    totalPrice = {totalPrice}>

    </OrderSummary>
    /*if (requestSent === true) {
        orderSummary = <Spinner/>
    }*/
    Object.keys(ingredients).forEach(elem => { disabledInfo[elem] = (ingredients[elem] <=0)})
    return (
        <Aux>
            <Modal show = {purchasing}
            backdropClickedHandler = {cancelPurchaseHandler}>
                
                {orderSummary}
                

            </Modal>

            <Burger ingredients = {ingredients}></Burger>
            <BuildControls price = {totalPrice} ingredientAdded = {onAddIngredient}
            ingredientRemoved = {onRemoveIngredient}
            disabledInfo = {disabledInfo}
            purchasable = {purchasable}
            purchaseHandler = {purchaseHandler}
            isAuth = {token != null}> 
            > </BuildControls>
            
        
        </Aux>
    );
    
}
const mapStateToProps = (state) =>{
    return {
        /*ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        purchasable: state.burgerBuilder.purchasable,
        loadError: state.burgerBuilder.error,
        token: state.auth.token*/
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
       /* onAddIngredient: (ingredient)=> dispatch(actionCreators.addIngredient(ingredient)),
        onRemoveIngredient: (ingredient)=> dispatch(actionCreators.removeIngredient(ingredient)),
        onInitIngredients: ()=> dispatch(actionCreators.initIngredientsStart()),
        onPurchaseInit: ()=>dispatch(actionCreators.purchaseInit()),
        onAuthSetRedirectPath: (path)=> dispatch(actionCreators.authSetRedirectPath(path))*/
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(burgerBuilder, Axios));

//export default withErrorHandler(BurgerBuilder, Axios);