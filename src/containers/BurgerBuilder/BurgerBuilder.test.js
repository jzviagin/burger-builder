import {BurgerBuilder} from './BurgerBuilder';

import React from 'react';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'

import BuildControls from '../../components/burger/BuildConrols/BuildControls'


configure({adapter: new Adapter()});
describe('<BurgerBuilder/ >', () =>{

    let wrapper;
    beforeEach(()=>{

        /*
              this.props.onInitIngredients()
        this.props.onAuthSetRedirectPath('/');*/
        wrapper = shallow(<BurgerBuilder onInitIngredients = {()=>{}} onAuthSetRedirectPath = {(path)=>{}}/>);
    })

    it('should render build controls when receiving ingredients', ()=>{
        wrapper.setProps({
            ingredients: {
                
                    "bacon" : 0,
                    "cheese" : 0,
                    "meat" : 0,
                    "salad" : 0
                  
                  
            }
        });
        expect(wrapper.find(BuildControls )).toHaveLength(1);
    });
    
})