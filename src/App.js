import React, { useEffect, Suspense } from 'react';

import Layout from './hoc/layout/Layout'
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder'
import Logout from './containers/Auth/Logout/Logout'

import {Route, Switch} from 'react-router-dom'



import * as actionCreators from './store/actions/'

import {connect} from 'react-redux'





const app = (props) => {

  /*componentDidMount(){
    this.props.onAuthCheckState();
  }*/

  const {onAuthCheckState} = props;

  useEffect(()=>{
    onAuthCheckState();
  } ,[onAuthCheckState])
  

  let routes = null;
  if (props.token !=null){
    routes = (

      <Switch>
        <Route path="/login" component ={React.lazy(()=>{
          return import('./containers/Auth/Auth')
        })} />
        <Route path="/logout" component ={Logout} />
        <Route path="/orders" component ={React.lazy(()=>{
          return import('./containers/Orders/Orders')
        })} />
        <Route path="/checkout" component ={React.lazy(()=>{
          return import('./containers/Checkout/Checkout')
        })} />
      <Route path="/" exact component ={BurgerBuilder} />   
      </Switch>

    )
  
  }else{
    routes = (

      <Switch>
        <Route path="/login" component ={React.lazy(()=>{
          return import('./containers/Auth/Auth')
        })} />
        <Route path="/" exact component ={BurgerBuilder} />
      </Switch>

    )
  }
  return (
    
    <Layout>
      <Suspense fallback = {<p>Loading...</p>}>
        {routes}
      </Suspense>
    </Layout>
  );

}


const mapStateToProps = (state) =>{

  return {
      token : state.auth.token 
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      onAuthCheckState: () => dispatch(actionCreators.authCheckState())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(app);


