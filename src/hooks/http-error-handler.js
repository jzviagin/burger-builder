import {useState, useEffect} from 'react'


export default httpClient => {

    const [error,setError] = useState(null);

    console.log('error-handler mounted');
    const responseInterceptor = httpClient.interceptors.response.use(res=>res,  (err) =>{
            setError(err);   
            Promise.reject(error);
        });
    useEffect(()=>{

        return ()=>{
            console.log('error-handler ejected');
            httpClient.interceptors.response.eject(responseInterceptor);
        }
        
    },[responseInterceptor]);

        
    const errorConfirmedHandler = ()=>{
        setError(null)
    }
    
  
    return [error, errorConfirmedHandler]

    

}